﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TrainsCSharp.Algorithms;
using TrainsCSharp.Interfaces;

namespace TrainsCSharp.Tests
{
    [ExcludeFromCodeCoverage]
    [TestFixture]
    public class BreadthFirstSearchTests : AlgorithmsTestBase
    {
        //[TestCase("C", "C", 3, ExpectedResult = 2)] //Works but temporarily disabled due to what seems to be a parallel execution bug with Resharper/NUnit
        [TestCase("A", "D", 2, ExpectedResult = 1)]
        public int GetAllPathsUnderMaxNodes(string origin, string destination, int maxNodes)
        {
            List<List<INode>> paths = BreadthFirstSearch.GetAllPathsUnderMaxNodes(Graph, origin, destination, maxNodes);
            return paths.Count;
        }

        [TestCase("C", "C", 30, ExpectedResult = 7)]
        //[TestCase("A", "D", 20, ExpectedResult = 2)] //Works but temporarily disabled due to what seems to be a parallel execution bug with Resharper/NUnit
        public int GetAllPathsUnderMaxLength(string origin, string destination, int maxLength)
        {
            List<List<INode>> paths = BreadthFirstSearch.GetAllPathsUnderMaxLength(Graph, origin, destination, maxLength);
            return paths.Count;
        }
    }

}