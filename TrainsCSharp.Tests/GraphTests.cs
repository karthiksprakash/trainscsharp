﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TrainsCSharp.Interfaces;

namespace TrainsCSharp.Tests
{
    [ExcludeFromCodeCoverage]
    [TestFixture]
    public class GraphTests : AlgorithmsTestBase
    {
        [TestCase("AB5, BC6", ExpectedResult = 3)]
        [TestCase("AB5, BC6, AK6", ExpectedResult = 4)]
        public int GraphInitializesWithRouteDumpAndReturnsNodeCount(string edgeDump)
        {
            IGraph graph = new Graph();
            graph.InitializeEdges(edgeDump);
            return graph.NodeCount;
        }

        [TestCase("AB5, B6")]
        [TestCase("ABB, AB6")]
        [ExpectedException(typeof(ArgumentException))]
        public void GraphInitialize_BadRouteSpecThrowsError(string edgeDump)
        {
            IGraph graph = new Graph();
            graph.InitializeEdges(edgeDump);
        }

        [TestCase("A-B-C", ExpectedResult = "9")]
        [TestCase("A-D", ExpectedResult = "5")]
        [TestCase("A-D-C", ExpectedResult = "13")]
        [TestCase("A-E-B-C-D", ExpectedResult = "22")]
        [TestCase("A-E-D", ExpectedResult = "NO SUCH ROUTE")]
        public string GetRouteDistance(string route)
        {
            string result = Graph.GetDistanceOfRoute(route);
            Console.WriteLine(result);
            return result;
        }

        [TestCase("A-B-C", ExpectedResult = 9)]
        [TestCase("A-D", ExpectedResult = 5)]
        [TestCase("A-D-C", ExpectedResult = 13)]
        [TestCase("A-E-B-C-D", ExpectedResult = 22)]
        [TestCase("A-E-D", ExpectedResult = int.MaxValue)]
        public int GetRouteDistanceForOrderedNodeList(string route)
        {

            int result = Graph.GetDistanceOfRoute(Graph.GetNodesInRoute(route));
            Console.WriteLine(result);
            return result;
        }

        [TestCase("A-B-C", ExpectedResult = 11)]
        [TestCase("A-E-D", ExpectedResult = int.MaxValue)]
        public int GetRouteDuration(string route)
        {
            int result = Graph.GetDurationOfRoute(route);
            Console.WriteLine(result);
            return result;
        }

        [TestCase("A-B-C", ExpectedResult = 3)]
        public int GetNodesInRoute(string route)
        {
            List<INode> nodes = Graph.GetNodesInRoute(route);
            //Console.WriteLine(result);
            return nodes.Count;
        }

    }
}
