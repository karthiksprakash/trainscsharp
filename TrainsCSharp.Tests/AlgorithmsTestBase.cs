﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TrainsCSharp.Interfaces;

namespace TrainsCSharp.Tests
{
    [ExcludeFromCodeCoverage]
    public abstract class AlgorithmsTestBase
    {
        protected readonly IGraph Graph = new Graph();

        [SetUp]
        public void Setup()
        {
            Graph.InitializeEdges("AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7");
        }
    }
}
