﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TrainsCSharp.Interfaces;

namespace TrainsCSharp.Tests
{
    [ExcludeFromCodeCoverage]
    [TestFixture]
    public class NodeTests : AlgorithmsTestBase
    {
        [TestCase("A", "B", 2)]
        public void AddEdge(string origin, string destination, int distance)
        {
            INode originNode = new Node(origin);
            INode destinationNode = new Node(destination);

            originNode.AddEdge(destinationNode, distance);

            Assert.IsTrue(originNode.Edges.Count == 1, "One edge was expected to be added.");
            IEdge edge = originNode.Edges.First();
            Assert.IsTrue(edge.Origin.Name.Equals(origin), $"Expected origin node {origin}, but was {edge.Origin.Name}");
            Assert.IsTrue(edge.Destination.Name.Equals(destination), $"Expected origin node {destination}, but was {edge.Destination.Name}");
            Assert.IsTrue(edge.Distance == distance, $"Expected distance from {origin} to {destination} to be {distance}, but was {edge.Distance}");
        }

        [TestCase("A", "A", 2, ExpectedExceptionName = "System.ArgumentException")]
        public void AddEgde_Same_Origin_And_Destination_ThrowsException(string origin, string destination, int distance)
        {
            INode originNode = new Node(origin);
            INode destinationNode = new Node(destination);

            originNode.AddEdge(destinationNode, distance);
        }

        [TestCase("B", "C", ExpectedResult = 4)]
        [TestCase("B", "D", ExpectedResult = int.MaxValue)]
        public int GetDistanceToConnectedNode(string origin, string destination)
        {
            INode destinationNode = Graph.GetNode(destination);
            return Graph.GetNode(origin).GetDistanceToConnectedNode(destinationNode);
        }

        //[TestCase("B")]
        //public int GetAdjacentNodes(string originNode)
        //{
        //    INode node = Graph.GetNode(originNode);
        //    var adjacentNodes = node.GetAdjacentNodes();
        //    Assert.IsTrue(adjacentNodes.Count > 0, "Expected some adjacent nodes.");
        //}
    }
}
