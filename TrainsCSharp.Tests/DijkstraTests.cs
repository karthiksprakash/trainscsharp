﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TrainsCSharp.Algorithms;
using TrainsCSharp.Interfaces;

namespace TrainsCSharp.Tests
{
    [ExcludeFromCodeCoverage]
    [TestFixture]
    public class DijkstraTests : AlgorithmsTestBase
    {
        [TestCase("A", "C", ExpectedResult = 9)]
        [TestCase("E", "D", ExpectedResult = 15)]
        public int GetShortestDistance(string origin, string destination)
        {
            INode originNode = Graph.GetNode(origin);
            INode destinationNode = Graph.GetNode(destination);

            int distance = Dijkstra.GetShortestDistance(Graph, originNode, destinationNode);
            Console.WriteLine(distance);
            return distance;
        }
    }
}
