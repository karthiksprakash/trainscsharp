﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainsCSharp.Interfaces;

namespace TrainsCSharp
{
    public class Edge : IEdge
    {
        //The origin town
        public INode Origin { get; }

        //The destination town
        public INode Destination { get; }

        //Distance to destination
        public int Distance { get; }

        public Edge(INode origin, INode destination, int distance)
        {
            Origin = origin;
            Destination = destination;
            Distance = distance;
        }

        public override string ToString()
        {
            return $"{Origin}{Destination}{Distance}";
        }
    }
}
