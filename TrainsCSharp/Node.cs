﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainsCSharp.Interfaces;

namespace TrainsCSharp
{
    public class Node : INode
    {
        public List<IEdge> Edges { get; private set; }

        public string Name { get; }

        public Node(string name)
        {
            Name = name;
            Edges = new List<IEdge>();
        }

        /// <summary>
        /// Creates a new edge originating from this node and returns it.
        /// </summary>
        public IEdge AddEdge(INode destination, int distance)
        {
            if (destination.Equals(this))
                throw new ArgumentException("Destination cannot be the same as Origin. The starting and ending town cannot be the same.");
            IEdge edge = new Edge(this, destination, distance); //TODO: Create and use factory
            return AddEdge(edge);
        }

        /// <summary>
        /// Adds an edge to this node and returns it.
        /// </summary>
        public IEdge AddEdge(IEdge edge)
        {
            if (!edge.Origin.Equals(this))
                throw new ArgumentException("Cannot add the edge since its Origin is not the same as current node.");
            if (!Edges.Contains(edge)) //Check if edge already exists
                Edges.Add(edge);
            return edge;
        }

        /// <summary>
        /// Returns a list of adjacent nodes
        /// </summary>
        public List<INode> GetAdjacentNodes()
        {
            return Edges.Select(e => e.Destination).ToList();
        }

        public int GetDistanceToConnectedNode(INode node)
        {
            IEdge connectedEdge = Edges.FirstOrDefault(e => e.Destination.Equals(node));
            return connectedEdge?.Distance ?? int.MaxValue;
        }

        public override bool Equals(object obj)
        {
            // If parameter is null return false.
            // If parameter cannot be cast to Node return false.
            INode p = obj as INode;
            if (p == null)
            {
                return false;
            }

            // Return true if the name match:
            return string.Equals(Name, p.Name, StringComparison.InvariantCultureIgnoreCase);
        }

        public virtual bool Equals(INode other)
        {
            return string.Equals(Name, other.Name, StringComparison.InvariantCultureIgnoreCase);
        }

        public override int GetHashCode()
        {
            return Name?.GetHashCode() ?? 0;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
