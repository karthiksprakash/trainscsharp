﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainsCSharp.Interfaces;

namespace TrainsCSharp.Algorithms
{
    public static class Dijkstra
    {
        /// <summary>
        /// Gets the shortest distance between two nodes in the given graph
        /// A return value of Int.MaxValue implies that no route exists between the nodes. 
        /// </summary>
        public static int GetShortestDistance(IGraph graph, INode origin, INode destination)
        {
            List<INode> unvisited = new List<INode>();
            List<INode> visited = new List<INode>();

            List<INode> nodesInGraph = graph.GetAllNodes();
            Dictionary<string, int> distanceMap = new Dictionary<string, int>();

            // Start by setting the distances from all nodes to the start INode to infinity
            nodesInGraph.ForEach(n => distanceMap.Add(n.Name, int.MaxValue));

            // Add origin to the list of unvisited nodes and set the distance to 0 
            unvisited.Add(origin);
            distanceMap[origin.Name] = 0;

            while (unvisited.Any())
            {
                // Find the nearest INode in the unvisited list 
                INode nearest = null;
                int minDistance = int.MaxValue;
                foreach (INode n in unvisited)
                {
                    int currentdistance = distanceMap[n.Name];
                    if (currentdistance < minDistance)
                    {
                        nearest = n;
                        minDistance = currentdistance;
                    }
                }

                // Move the nearest INode to the list of visited nodes
                unvisited.Remove(nearest);
                visited.Add(nearest);

                // Now for all unvisited neighbors of the current INode, map the distance.
                // Find the nodes for which we find a shorter path to the origin than the ones we had previously calculated for 
                foreach (IEdge edge in nearest.Edges.Where(e => !visited.Contains(e.Destination)))
                {
                    int newDistance = distanceMap[nearest.Name] + edge.Distance;
                    int dist = distanceMap[edge.Destination.Name];
                    if (newDistance < dist)
                    {
                        // Save the new shortest distance and move it to the list of unvisited nodes. 
                        distanceMap[edge.Destination.Name] = newDistance;
                        unvisited.Add(edge.Destination);
                    }
                }

                // If the target INode has already been visited, we can stop searching
                if (visited.Contains(destination))
                {
                    break;
                }
            }
            // returns the shortest distance recorded from the target INode to the start INode
            return distanceMap[destination.Name];
        }
    }
}
