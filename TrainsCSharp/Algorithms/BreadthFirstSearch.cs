﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainsCSharp.Interfaces;

namespace TrainsCSharp.Algorithms
{
    public static class BreadthFirstSearch
    {
        /// <summary>
        /// Gets all paths between two given nodes in a graph upto a given max number of nodes
        /// </summary>
        public static List<List<INode>> GetAllPathsUnderMaxNodes(IGraph graph, string origin, string destination, int maxNodes = int.MaxValue)
        {
            List<List<INode>> paths = new List<List<INode>>();
            LinkedList<INode> visited = new LinkedList<INode>();
            visited.AddLast(graph.GetNode(origin));
            GetAllPathsHelper(graph.GetNode(destination), visited, x => paths.Add(new List<INode>(x)),
                x => x.Count == maxNodes);
            return paths;
        }

        /// <summary>
        /// Gets all paths between two given nodes in a graph where the max path length does not exceed a given value.
        /// </summary>
        public static List<List<INode>> GetAllPathsUnderMaxLength(IGraph graph, string origin, string destination, int maxPathLength = int.MaxValue)
        {
            List<List<INode>> paths = new List<List<INode>>();
            LinkedList<INode> visited = new LinkedList<INode>();
            visited.AddLast(graph.GetNode(origin));
            GetAllPathsHelper(graph.GetNode(destination), visited, x => paths.Add(new List<INode>(x)),
                x => graph.GetDistanceOfRoute(x.ToList()) > maxPathLength);
            return paths.Where(p => graph.GetDistanceOfRoute(p) < maxPathLength).ToList();
        }
        
        /// <summary>
        /// Implements a modified BFS algorithm
        /// </summary>
        private static void GetAllPathsHelper(INode destination, LinkedList<INode> visited, Action<List<INode>> pathRecorder, Func<LinkedList<INode>, bool> shouldStopSearch)
        {
            List<INode> adjacentNodes = visited.Last().GetAdjacentNodes();

            foreach (INode INode in adjacentNodes)
            {
                //if (visited.Contains(INode))
                //   continue;
                if (INode.Equals(destination))
                {
                    visited.AddLast(INode);
                    pathRecorder(visited.ToList());
                    visited.RemoveLast();
                    break;
                }
            }

            if (shouldStopSearch(visited)) return;

            foreach (INode INode in adjacentNodes)
            {
                //if (visited.Contains(INode) || INode.Equals(destination))
                //   continue;
                visited.AddLast(INode);
                GetAllPathsHelper(destination, visited, pathRecorder, shouldStopSearch);
                visited.RemoveLast();
            }
        }
    }
}
