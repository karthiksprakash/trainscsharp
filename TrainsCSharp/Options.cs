﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;

namespace TrainsCSharp
{
    class Options
    {
        [Option('g', "graphFile", DefaultValue = "GraphSpec.txt",
            HelpText = "Path to the file containing the graph specification")]
        public string GraphSpecFile { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this, (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}
