﻿using TrainsCSharp.Interfaces;

namespace TrainsCSharp
{
    public interface IEdge
    {
        INode Destination { get; }
        int Distance { get; }
        INode Origin { get; }

        string ToString();
    }
}