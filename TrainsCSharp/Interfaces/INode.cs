﻿using System.Collections.Generic;

namespace TrainsCSharp.Interfaces
{
    public interface INode
    {
        List<IEdge> Edges { get; }
        string Name { get; }

        IEdge AddEdge(IEdge edge);
        IEdge AddEdge(INode destination, int distance);
        bool Equals(INode other);
        bool Equals(object obj);
        List<INode> GetAdjacentNodes();
        /// <summary>
        /// Gets the distance to the node if connected. Returns int.MaxValue otherwise.
        /// </summary>
        int GetDistanceToConnectedNode(INode node);
        int GetHashCode();
        string ToString();
    }
}