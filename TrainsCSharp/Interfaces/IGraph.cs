﻿using System.Collections.Generic;

namespace TrainsCSharp.Interfaces
{
    public interface IGraph
    {
        int NodeCount { get; }

        void AddEdge(string edge);
        List<INode> GetAllNodes();
        int GetDistanceOfRoute(List<INode> route);
        string GetDistanceOfRoute(string route);
        INode GetNode(string name);
        List<INode> GetNodesInRoute(string route);
        void InitializeEdges(string edgeDump);
        /// <summary>
        /// Gets the duration of the route
        /// </summary>
        int GetDurationOfRoute(string route);
    }
}