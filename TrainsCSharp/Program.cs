﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TrainsCSharp.Algorithms;
using TrainsCSharp.Interfaces;

namespace TrainsCSharp
{
    internal class Program
    {
        private const string RegexToValidateCommaSeparatedValues = @"^[-\w\s]+(?:,[-\w\s]*)*$";

        private static void Main(string[] args)
        {
            var options = new Options();
            string useDefaultsReason = string.Empty;
            string graphSpec = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";

            if (args.Any())
            {
                if (CommandLine.Parser.Default.ParseArguments(args, options))
                {
                    graphSpec = GetGraphSpecFromFile(options.GraphSpecFile);
                    if (string.IsNullOrWhiteSpace(graphSpec) ||
                        !Regex.IsMatch(graphSpec, RegexToValidateCommaSeparatedValues))
                        useDefaultsReason = "No valid graph specification found.";
                }
                else
                {
                    useDefaultsReason = "Invalid options specified.";
                    Console.WriteLine("Invalid options specified.");
                }
            }
            else
            {
                useDefaultsReason = "No options specified.";
            }
            if (!string.IsNullOrEmpty(useDefaultsReason)) Console.WriteLine(useDefaultsReason + " Using defaults.");

            IGraph graph = new Graph();
            graph.InitializeEdges(graphSpec);
            SolveInputs(graph);

            Console.WriteLine("\n\nPress any key to exit...");
            Console.ReadKey();
        }

        /// <summary>
        /// Answers specific questions
        /// </summary>
        private static void SolveInputs(IGraph graph)
        {
            Console.WriteLine($"1) The distance of the route A-B-C  =  {graph.GetDistanceOfRoute("A-B-C")}\n");
            Console.WriteLine($"2) The distance of the route A-D  =  {graph.GetDistanceOfRoute("A-D")}\n");
            Console.WriteLine($"3) The distance of the route A-D-C  =  {graph.GetDistanceOfRoute("A-D-C")}\n");
            Console.WriteLine($"4) The distance of the route A-E-B-C-D  =  {graph.GetDistanceOfRoute("A-E-B-C-D")}\n");
            Console.WriteLine($"5) The distance of the route A-E-D  =  {graph.GetDistanceOfRoute("A-E-D")}\n");
            
            List<List<INode>> paths = BreadthFirstSearch.GetAllPathsUnderMaxNodes(graph, "C", "C", 3);
            Console.WriteLine($"6) The number of trips starting at C and ending at C with a maximum of 3 stops. {paths.Count}");
            PrintNodePathsAsRoutes(paths);

            paths = BreadthFirstSearch.GetAllPathsUnderMaxNodes(graph, "A", "C", 4).Where(p => p.Count == 5).ToList();
            Console.WriteLine($"7) The number of trips starting at A and ending at C with exactly 4 stops. {paths.Count}");
            PrintNodePathsAsRoutes(paths);

            Console.WriteLine($"8) The length of the shortest route (in terms of distance to travel) from A to C  =  {GetShortestDistance(graph, "A", "C")}\n");
            Console.WriteLine($"9) The length of the shortest route (in terms of distance to travel) from B to B  =  {GetShortestDistance(graph, "B", "B")}\n");

            paths = BreadthFirstSearch.GetAllPathsUnderMaxLength(graph, "C", "C", 30);
            Console.WriteLine($"10) The number of different routes from C to C with a distance of less than 30  =  {paths.Count}");
            PrintNodePathsAsRoutes(paths);
        }



        /// <summary>
        /// Returns the comma separated graph spec as a string from given the file path
        /// </summary>
        private static string GetGraphSpecFromFile(string filename)
        {
            string graphSpec = string.Empty;
            try
            {
                StreamReader reader = new StreamReader(filename);
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    Match match = Regex.Match(line, @"^\[(.+)\]");
                    if (match.Success)
                    {
                        graphSpec = match.Groups[1].Value;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                //Ideally, a logging framework would be used here. But skipping that for now.
                Console.WriteLine($"ERROR: Unable to read the graph file. {e.Message}");
            }
            return graphSpec;
        }

        /// <summary>
        /// Runs the Dijkstra algorithm to get the shortest distance
        /// </summary>
        private static string GetShortestDistance(IGraph graph, string origin, string destination)
        {
            int shortestDistance = int.MaxValue;
            INode originNode = graph.GetNode(origin);
            INode destinationNode = graph.GetNode(destination);
            if (origin.Equals(destination)) //Special case where origin equals destination
            {
                foreach (IEdge edge in originNode.Edges)
                {
                    int distance = Dijkstra.GetShortestDistance(graph, edge.Destination, destinationNode);
                    if (distance != int.MaxValue)
                    {
                        // a path has been found from a neighbor of the start INode
                        // to the start INode. Total distance will of course include
                        // the distance from the start INode to this neighbor.
                        distance += edge.Distance;
                        if (distance < shortestDistance)
                        {
                            // a new shortest distance has been found
                            shortestDistance = distance;
                        }
                    }
                }
            }
            else
            {
                shortestDistance = Dijkstra.GetShortestDistance(graph, originNode, destinationNode);
            }

            return shortestDistance != int.MaxValue ? shortestDistance.ToString() : "NO SUCH ROUTE";
        }


        #region DisplayHelpers

        private static string GetPathAsRouteString(List<INode> nodes)
        {
            return string.Join("-", nodes.Select(p => p.Name));
        }

        private static void PrintNodePathsAsRoutes(List<List<INode>> paths)
        {
            paths.ForEach(p => Console.WriteLine(GetPathAsRouteString(p)));
        }

        #endregion
    }
}
