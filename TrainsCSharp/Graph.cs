﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TrainsCSharp.Interfaces;

namespace TrainsCSharp
{
    public class Graph : IGraph
    {
        private readonly Dictionary<string, INode> _nodeMap;

        public int NodeCount => _nodeMap.Count();

        public Graph()
        {
            _nodeMap = new Dictionary<string, INode>();
        }

        /// <summary>
        /// Initialize the graph with a comma separated list of edges. Ex. "AB5, BC4, CD8"
        /// </summary>
        public void InitializeEdges(string edgeDump)
        {
            edgeDump.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(r => AddEdge(r.Trim()));
        }

        /// <summary>
        /// Adds an edge (ex. "AB5") to the graph
        /// </summary>
        public void AddEdge(string edge)
        {
            // An edge spec must be at least 3 characters.. i.e. AB5
            if (edge.Length < 3)
            {
                throw new ArgumentException($"Bad edge specification \'{edge}\'");
            }

            string originNodeName = edge.Substring(0, 1);
            string destinationNodeName = edge.Substring(1, 1);

            int distance;
            string distanceInSpec = edge.Substring(2);
            if (!int.TryParse(distanceInSpec, out distance))
                throw new ArgumentException($"Bad edge specification. Invalid distance \'{distanceInSpec}\'");

            INode originNode = AddNode(originNodeName);
            INode destinationNode = AddNode(destinationNodeName);

            originNode.AddEdge(destinationNode, distance);
        }

        /// <summary>
        /// Gets the Node with the given name.
        /// </summary>
        /// <returns>Node with the given name or null if it is not found.</returns>
        public INode GetNode(string name)
        {
            return _nodeMap.ContainsKey(name.ToUpper()) ? _nodeMap[name] : null;
        }

        /// <summary>
        /// Gets a list of all nodes in the graph
        /// </summary>
        public List<INode> GetAllNodes()
        {
            return _nodeMap.Values.ToList();
        }

        /// <summary>
        /// If the node does not already exists, creates a new Node and adds it to the graph. 
        /// Finally, returns the existing or newly added Node
        /// </summary>
        private INode AddNode(string name)
        {
            if (_nodeMap.ContainsKey(name.ToUpper())) return _nodeMap[name];
            else
            {
                INode newNode = new Node(name.ToUpper()); //TODO: Create and use factory
                _nodeMap.Add(newNode.Name, newNode);
                return newNode;
            }
        }

        /// <summary>
        /// Gets the list of nodes in a route specification (Ex. "A-B-C" will return a list of nodes contains A, B and C)
        /// </summary>
        public List<INode> GetNodesInRoute(string route)
        {
            if (string.IsNullOrEmpty(route) || !Regex.IsMatch(route, @"^[A-Z]{1}(?:-[A-Z]{1})+$"))
                throw new ArgumentException($"The route specification \"{route}\" is invalid", nameof(route));

            List<string> nodeNames = route.Split(new[] {'-'}, StringSplitOptions.RemoveEmptyEntries)
                .Select(n => n.Trim())
                .ToList();

            List<INode> nodes = new List<INode>();
            nodeNames.ForEach(n =>
            {
                INode node = GetNode(n);
                if (node != null) nodes.Add(node);
                else throw new ArgumentException($"The node \'{n}\' was not found in the graph.", nameof(route));
            });

            return nodes;
        }

        /// <summary>
        /// Gets the duration of the route
        /// </summary>
        public int GetDurationOfRoute(string route)
        {
            List<INode> nodesInRoute = GetNodesInRoute(route);
            if (nodesInRoute.Count < 2)
                throw new ArgumentException($"The route \"{route}\" does not have at-least two nodes.");

            int totalDuration = (nodesInRoute.Count - 2) * 2;

            int totalDistance = GetDistanceOfRoute(nodesInRoute);
            if (totalDistance != int.MaxValue)
                totalDuration += totalDistance * 1;
            else
                totalDuration = int.MaxValue;
            
            return totalDuration;
        }


        /// <summary>
        /// Gets the distance to be traveled in the route
        /// </summary>
        public string GetDistanceOfRoute(string route)
        {
            List<INode> nodesInRoute = GetNodesInRoute(route);
            if (nodesInRoute.Count < 2)
                throw new ArgumentException($"The route \"{route}\" does not have at-least two nodes.");

            int totalDistance = GetDistanceOfRoute(nodesInRoute);
            return (totalDistance != int.MaxValue) ? totalDistance.ToString() : "NO SUCH ROUTE";
        }

        /// <summary>
        /// Gets the distance to be traveled in the route represented by in-order list of nodes in the route
        /// </summary>
        public int GetDistanceOfRoute(List<INode> route)
        {
            int totalDistance = 0;
            INode currentNode = route[0]; //Start with the first node in order
            for (int i = 1; i < route.Count; i++)
            {
                int distanceToNextNode = currentNode.GetDistanceToConnectedNode(route[i]);
                if (distanceToNextNode == int.MaxValue)
                {
                    //A dead-end in the route chain
                    totalDistance = int.MaxValue;
                    break;
                }

                totalDistance += distanceToNextNode;

                //Traverse down the route
                currentNode = route[i];
            }

            return totalDistance;
        }

    }
}
